const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const config = require("config");
const jwt = require("jsonwebtoken");

// User Model
const User = require("../../models/User");

// @route  GET api/users
// @desc   Register new user
// @access Public
router.post("/", (req, res) => {
  const { name, email, password } = req.body;
  // Intermediate Validation
  if (!name && !email && !password) {
    // 400 = Bad Request
    return res.status(400).json({ msg: "Please enter all fields" });
  }
  if (!name) {
    return res.status(400).json({ msg: "Please enter a valid name" });
  }

  if (!email) {
    return res.status(400).json({ msg: "Please enter a valid email" });
  }

  if (!password) {
    return res.status(400).json({ msg: "Please enter a valid password" });
  }

  // Check for password of appropriate length
  if (password.length < 8 || password.length > 20) {
    return res.status(400).json({
      msg: "Please create a password between 8 and 20 characters in length",
    });
  }

  // Check for existing user
  User.findOne({ email }).then((user) => {
    if (user) {
      return res.status(400).json({ msg: "User already exists" });
    }
    const newUser = new User({
      name,
      email,
      password,
    });

    // Create salt & hash
    bcrypt.genSalt(13, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) {
          throw err;
        }
        newUser.password = hash;
        newUser.save().then((user) => {
          jwt.sign(
            {
              id: user.id,
            },
            config.get("jwtSecret"),
            // Expires in 2 hours
            { expiresIn: 7200 },
            (err, token) => {
              if (err) throw err;
              res.json({
                token,
                user: {
                  id: user.id,
                  name: user.name,
                  email: user.email,
                },
              });
            }
          );
        });
      });
    });
  });
});

module.exports = router;
