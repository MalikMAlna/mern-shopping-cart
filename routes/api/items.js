const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");

// Item Model
const Item = require("../../models/Item");

// @route  GET api/items
// @desc   Get All Items
// @access Public
router.get("/", (req, res) => {
  Item.find()
    .sort({ date: -1 })
    .then((items) => res.json(items));
});

// @route  POST api/items
// @desc   Create An Item
// @access Private
router.post("/", auth, (req, res) => {
  const newItem = new Item({ name: req.body.name });

  newItem.save().then((item) => res.json(item));
});

// @route  DELETE api/items
// @desc   Delete An Item
// @access Private
router.delete("/:id", auth, (req, res) => {
  const deletedItem = req.params.id;
  Item.findById(deletedItem)
    .then((item) =>
      item.remove().then(() =>
        res.json({
          result: `Item with ID: ${deletedItem} successfully removed!`,
        })
      )
    )
    .catch((err) =>
      res.status(404).json({
        result: `Item with ID: ${deletedItem} failed to be removed!`,
      })
    );
});

module.exports = router;
