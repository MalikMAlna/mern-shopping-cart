import App from "./components/App"
import { Provider } from "react-redux";
import store from "./store";

const AppWrapper = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

export default AppWrapper;
