import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Container, ListGroup, ListGroupItem, Button } from "reactstrap";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { getItems, deleteItem } from "../actions/itemActions";

const ShoppingCart = () => {
  const items = useSelector((state) => {
    console.log(state);
    return state.cart.items;
  });
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getItems());
  }, [dispatch]);

  const deleteCartItem = (id) => dispatch(deleteItem(id));

  const isAuthenticated = useSelector((state) => {
    console.log(state);
    return state.auth.isAuthenticated;
  });

  return (
    <Container>
      <ListGroup>
        <TransitionGroup className="shopping-cart">
          {items.map(({ _id, name }) => (
            <CSSTransition key={_id} timeout={500} classNames="fade">
              <ListGroupItem>
                {isAuthenticated ? (
                  <Button
                    className="remove-btn"
                    color="danger"
                    size="sm"
                    onClick={() => {
                      deleteCartItem(_id);
                    }}
                  >
                    &times;
                  </Button>
                ) : null}

                {name}
              </ListGroupItem>
            </CSSTransition>
          ))}
        </TransitionGroup>
      </ListGroup>
    </Container>
  );
};

export default ShoppingCart;
