import AppNavbar from "./AppNavbar";
import ShoppingCart from "./ShoppingCart";
import ItemModal from "./ItemModal";
import { Container } from "reactstrap";

import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { loadUser } from "../actions/authActions";

import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadUser());
  }, [dispatch]);

  return (
    <>
      <AppNavbar />
      <Container>
        <ItemModal />
        <ShoppingCart />
      </Container>
    </>
  );
};

export default App;
