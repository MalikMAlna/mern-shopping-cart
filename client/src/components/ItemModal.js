import { useState } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { addItem } from "../actions/itemActions";

const ItemModal = () => {
  const [modal, setModal] = useState(false);
  const [name, setName] = useState("");
  const dispatch = useDispatch();
  const toggleModal = () => setModal((oldModal) => !oldModal);

  const isAuthenticated = useSelector((state) => {
    console.log(state);
    return state.auth.isAuthenticated;
  });

  const onChange = (e) => {
    const { value } = e.target;
    setName(value);
    console.log("value is:", value);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    console.log("name is:", name);
    if (name) {
      dispatch(addItem(name));
      toggleModal();
    }
  };

  return (
    <>
      {isAuthenticated ? (
        <Button
          color="info"
          style={{ marginBottom: "2rem" }}
          onClick={toggleModal}
        >
          Add Item
        </Button>
      ) : (
        <h4 className="mb-3 ml-4">Please login to modify items</h4>
      )}

      <Modal isOpen={modal} toggle={toggleModal}>
        <ModalHeader toggle={toggleModal}>Add To Shopping Cart</ModalHeader>
        <ModalBody>
          <Form onSubmit={onSubmit}>
            <FormGroup>
              <Label for="item">Item</Label>
              <Input
                type="text"
                name="name"
                id="item"
                placeholder="Add Item To Cart"
                onChange={onChange}
              />
              <Button color="info" style={{ marginTop: "2rem" }} block>
                Add Item
              </Button>
            </FormGroup>
          </Form>
        </ModalBody>
      </Modal>
    </>
  );
};

export default ItemModal;
