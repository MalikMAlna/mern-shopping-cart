import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Container,
} from "reactstrap";
import { useSelector } from "react-redux";
import RegisterModal from "./auth/RegisterModal";
import Logout from "./auth/Logout";
import LoginModal from "./auth/LoginModal";

const AppNavbar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  const isAuthenticated = useSelector((state) => {
    console.log(state);
    return state.auth.isAuthenticated;
  });

  const user = useSelector((state) => {
    console.log(state);
    return state.auth.user;
  });

  const authLinks = (
    <>
      <NavItem>
        <span className="navbar-text mr-3">
          <strong>
            {user
              ? user.name.includes(" ")
                ? `Hello ${
                    user.name.charAt(0).toUpperCase() +
                    user.name
                      .substr(1, user.name.indexOf(" ") - 1)
                      .toLowerCase()
                  }!`
                : `Hello ${
                    user.name.charAt(0).toUpperCase() +
                    user.name.substr(1).toLowerCase()
                  }!`
              : ""}
          </strong>
        </span>
      </NavItem>
      <NavItem>
        <Logout />
      </NavItem>
    </>
  );

  const guestLinks = (
    <>
      <NavItem>
        <RegisterModal />
      </NavItem>
      <NavItem>
        <LoginModal />
      </NavItem>
    </>
  );

  return (
    <div>
      <Navbar color="info" dark expand="sm" className="mb-5">
        <Container>
          <NavbarBrand href="/">ShoppingCart</NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ml-auto" navbar>
              {isAuthenticated ? authLinks : guestLinks}
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default AppNavbar;
