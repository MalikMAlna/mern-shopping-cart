import React from "react";
import { useDispatch } from "react-redux";
import { logout } from "../../actions/authActions";

import { NavLink } from "reactstrap";

const Logout = () => {
  const dispatch = useDispatch();

  const logoutUser = () => {
    dispatch(logout());
  };
  return (
    <>
      <NavLink onClick={logoutUser} href="#">
        Logout
      </NavLink>
    </>
  );
};

export default Logout;
