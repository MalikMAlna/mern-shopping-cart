import { useState, useEffect, useRef, useCallback } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  NavLink,
  Alert,
} from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../../actions/authActions";
import { clearErrors } from "../../actions/errorActions";

const LoginModal = () => {
  const [modal, setModal] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [msg, setMsg] = useState(null);

  const isAuthenticated = useSelector((state) => {
    return state.auth.isAuthenticated;
  });

  const error = useSelector((state) => {
    return state.error;
  });

  const prevErrorRef = useRef();
  useEffect(() => {
    prevErrorRef.current = error;
  });
  const prevError = prevErrorRef.current;

  const dispatch = useDispatch();

  const toggleModal = useCallback(() => {
    dispatch(clearErrors());
    setMsg(null);
    setModal((oldModal) => !oldModal);
  }, [dispatch]);

  useEffect(() => {
    if (error !== prevError) {
      if (error.id === "LOGIN_FAIL") {
        setMsg({ msg: error.msg.msg });
      }
    }
    // If authenticated, close modal
    if (modal) {
      if (isAuthenticated) {
        toggleModal();
      }
    }
  }, [error, prevError, setMsg, isAuthenticated, modal, toggleModal]);

  const onChangeEmail = (e) => {
    const { value } = e.target;
    setEmail(value);
  };

  const onChangePassword = (e) => {
    const { value } = e.target;
    setPassword(value);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const user = {
      email,
      password,
    };

    // Attempt Login
    dispatch(login(user));
  };
  return (
    <>
      <NavLink onClick={toggleModal} href="#">
        Login
      </NavLink>
      <Modal isOpen={modal} toggle={toggleModal}>
        <ModalHeader toggle={toggleModal}>Login To Your Account</ModalHeader>
        <ModalBody>
          {msg ? <Alert color="danger">{msg.msg}</Alert> : null}
          <Form onSubmit={onSubmit}>
            <FormGroup>
              <Label for="email">Email:</Label>
              <Input
                type="email"
                name="email"
                id="email"
                placeholder="Write your email here"
                className="mb-3"
                onChange={onChangeEmail}
              />
              <Label for="password">Password:</Label>
              <Input
                type="password"
                name="password"
                id="password"
                placeholder="Write your password here"
                className="mb-3"
                onChange={onChangePassword}
              />
              <Button color="info" style={{ marginTop: "2rem" }} block>
                Login
              </Button>
            </FormGroup>
          </Form>
        </ModalBody>
      </Modal>
    </>
  );
};

export default LoginModal;
