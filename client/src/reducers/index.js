import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import itemsReducer from "./itemsReducer";

export default combineReducers({
  auth: authReducer,
  error: errorReducer,
  cart: itemsReducer,
});
